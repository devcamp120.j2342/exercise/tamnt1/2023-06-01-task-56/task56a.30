﻿package com.example.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.example.Employee;

@Service
public class EmployeeService {
    public List<Employee> createEmployee() {
        ArrayList<Employee> employeeList = new ArrayList<>();

        Employee employe1 = new Employee(1, "tam", "ng", 20000);
        Employee employe2 = new Employee(1, "tam", "ng", 20000);
        Employee employe3 = new Employee(1, "tam", "ng", 20000);
        employeeList.add(employe1);
        employeeList.add(employe2);
        employeeList.add(employe3);

        return employeeList;

    }
}